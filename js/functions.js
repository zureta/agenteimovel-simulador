$('.range-valor').rangeslider({
    polyfill : false
});

$('.range-parcelas').rangeslider({
    polyfill : false
});

var valor = [
    60000, 65000, 70000, 75000, 80000, 85000, 90000, 95000, 100000, 110000, 
    120000, 130000, 140000, 150000, 160000, 170000, 180000, 190000, 200000, 250000, 
    300000, 350000, 400000, 450000, 500000, 550000, 600000, 650000, 700000, 750000, 
    800000, 850000, 900000, 950000, 1000000, 1050000, 1100000, 1150000, 1200000, 1250000, 
    1300000, 1350000, 1400000, 1450000, 1500000, 1550000, 1600000, 1650000, 1700000, 1750000, 
    1800000, 1850000, 1900000, 1950000, 2000000];
var parcelas = [36, 60, 96, 120, 180];

$('.range-parcelas').on('change input', function() {
    var value = parcelas[this.value];
    $("#total-parcelas").text(value + 'X');
}).change();

$('.range-valor').on('change input', function() {
    var value = valor[this.value];
    $("#total-valor").text('R$ ' + addCommas(value));
}).change();

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}