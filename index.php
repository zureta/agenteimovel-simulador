<!doctype html>
<html lang="pt-br">

    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Plugin's CSS -->
        <link rel="stylesheet" type="text/css" href="css/plugin/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/plugin/all.min.css">
        <link rel="stylesheet" type="text/css" href="css/plugin/rangeslider.css">

        <!-- Header e Footer Agente Imóvel -->
        <link rel="stylesheet" type="text/css" href="css/header-footer.css" />
        
        <!-- CSS separados -->
        <link rel="stylesheet" type="text/css" href="css/gerais.css" />
        <link rel="stylesheet" type="text/css" href="css/banner.css" />
        <link rel="stylesheet" type="text/css" href="css/icons.css" />
        <link rel="stylesheet" type="text/css" href="css/carrossel.css" />
        <link rel="stylesheet" type="text/css" href="css/entenda.css" />
        <link rel="stylesheet" type="text/css" href="css/diferencas.css" />
        <link rel="stylesheet" type="text/css" href="css/mitos.css" />
        <link rel="stylesheet" type="text/css" href="css/pros-contras.css" />
        <link rel="stylesheet" type="text/css" href="css/contratar.css" />
        <link rel="stylesheet" type="text/css" href="css/simulacao.css" />
        <link rel="stylesheet" type="text/css" href="css/documentacao.css" />
        <link rel="stylesheet" type="text/css" href="css/cuidados.css" />
        <link rel="stylesheet" type="text/css" href="css/parceiros.css" />
        <link rel="stylesheet" type="text/css" href="css/aviso.css" />

        <title>Financiamento - Agente Imóvel</title>

    </head>

    <body>
        <!-- HEADER -->
        <header>
            <img src="img/header.gif">
        </header>
        <!-- HEADER -->

        <!-- BANNER -->
        <div id="banner" class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 hold-tit">
                        <h1 class="title1 mb-5">
                            EMPRÉSTIMO COM IMÓVEL EM GARANTIA
                        </h1>
                    </div>
                    <div class="col-lg-8 ml-auto mr-auto">

                        <div class="box">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 pb-md-5">
                                    <h2>Simule seu empréstimo</h2>
                                    <div class="row">
                                        <div class="col-12 mb-3">
                                            <ul class="dados">
                                                <li><span class="label">Valor</span></li>
                                                <li><span id="total-valor">R$ 60.000</span></li>
                                            </ul>
                                            <input type="range" class="range-valor" value="0" min="0" max="54" step="1">
                                        </div>
                                    </div>
        
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="dados">
                                                <li><span class="label">Parcelas</span></li>
                                                <li><span id="total-parcelas">36X</span></li>
                                            </ul>
                                            <input type="range" class="range-parcelas" value="0" min="0" max="4" step="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 my-auto">
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit">
                                                Solicitar Proposta
                                            </button>
                                            <span class="taxa">
                                                Taxas de 1,15 a 2,99% ao mês
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- BANNER -->

        <!-- ÍCONES -->
        <div class="container-fluid" id="icons">
            <div class="container">

                <div class="row">
                    <div class="col-sm-6 col-lg-4">
                        <img class="img-icone icon01" src="img/ico-credito-economico.gif" alt="Icone de porcentagem." />
                        <h4>
                            O CRÉDITO MAIS ECONÔMICO
                        </h4>
                        <p>
                            Com juros muito abaixo da média, o crédito com imóvel em garantia é o mais barato em comparação com outras modalidades.
                        </p>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <img class="img-icone icon02" src="img/ico-credito.gif" alt="Icone de crédito." />
                        <h4>
                            ATÉ DOIS MILHÕES EM CRÉDITO
                        </h4>
                        <p>
                            As condições são flexíveis, além do alto valor de crédito, o prazo para pagamento é negociável, podendo chegar a até 20 anos.
                        </p>
                    </div>
                    <div class="col-sm-6 ml-sm-auto mr-sm-auto col-lg-4">
                        <img class="img-icone icon03" src="img/ico-processamento-rapido.gif" alt="Icone de casa." />
                        <h4>
                            PROCESSAMENTO RÁPIDO
                        </h4>
                        <p>
                            Com segurança e transparência cuidamos de todo o processo para disponibilizar a quantia o mais rápido possível.
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <!-- ÍCONES -->

        <!-- CARROSSEL -->
        <div class="container-fluid" id="carrossel">
            <div class="row">
                <div class="col-12 p-0 text-center">
                    <div class="cycle-slideshow"
                        data-cycle-fx="fade" 
                        data-cycle-timeout="10000"
                        data-cycle-pager=".cycle-pager"
                    >
                        <img src="img/depoimentos/slide01.jpg">
                        <img src="img/depoimentos/slide02.jpg">
                        <img src="img/depoimentos/slide03.jpg">
                    </div>
                    <div class="cycle-pager"></div>
                </div>
            </div>
        </div>
        <!-- CARROSSEL -->

        <!-- ENTENDA -->
        <div class="container" id="entenda">
            <div class="row">
                <div class="col-md-8 col-10 ml-auto mr-auto text-center">
                    <h3>Entenda o empréstimo com garantia de imóvel</h3>
                    <h4>
                        Amplie suas possibilidades e alcance seus objetivos com maior prazo para pagar e juros baixos
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-10 ml-auto mr-auto text-center mb-5">
                    <p>
                        O empréstimo com garantia de imóvel é a melhor opção para solicitar crédito com os menores juros praticados no mercado.
                    </p>
                    <p>
                        O crédito poderá ser utilizado para tornar realidade seus sonhos ou necessidades, como o refinanciamento de imóveis ou débitos, reformas, viagens, educação, investimentos e muito mais!
                    </p>
                    <p>
                        Conforme o desejo do favorecido a taxa é mais atrativa e a liberação do crédito se dá no máximo em 8 semanas.        
                    </p>
                    <p>
                        Abaixo, os principais aspectos do chamado Home Equity ou empréstimo com imóvel em garantia.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button class="btn-default">
                        Solicitar Proposta
                    </button>
                </div>
            </div>
        </div>
        <!-- ENTENDA -->

        <!-- DIFERENÇAS -->
        <div class="container-fluid" id="diferencas">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 ml-auto mr-auto mb-5">
                        <h3>Principais diferenças entre o empréstimo com garantia de imóvel e outras modalidades </h3>
                        <p>Para entender melhor as diferenças entre o empréstimo com garantia de imóvel e os mais populares disponíveis no mercado, é importante conhecer as principais opções hoje, são elas:</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10 ml-auto mr-auto">
                        <div class="row">
                            <div class="col-md-5 ml-auto mr-auto item">
                                <img src="img/ico-emprestimo-pessoal.gif" alt="Empréstimo pessoal" title="Empréstimo pessoal">
                                <p>
                                    <strong>Empréstimo pessoal</strong>
                                    Disponibilizado por bancos ou financeiras, por meio de contrato as instituições emprestam dinheiro para pessoa física sob análise de crédito e sem garantia. Taxa de juros média 4.7% ao mês.
                                </p>
                            </div>
                            <div class="col-md-5 ml-auto mr-auto item">
                                <img src="img/ico-financiamento.gif" alt="Financiamento" title="Financiamento">
                                <p>
                                    <strong>Financiamento</strong>
                                    Para compra de um bem específico (carro, casa), o valor é pago pela financiadora e parcelado para o beneficiado. Taxa de juros média 3.95% ao mês.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10 ml-auto mr-auto">
                        <div class="row">
                            <div class="col-md-5 ml-auto mr-auto item">
                                <img src="img/ico-cheque-especial.gif" alt="Cheque especial" title="Cheque especial">
                                <p>
                                    <strong>Cheque especial</strong>
                                    Semelhante ao empréstimo pessoal, porém sem a burocracia de solicitar o benefício ao banco. O valor é aprovado e fica disponível na conta, pode ser usado quando quiser e quitado no mês seguinte. Taxa de juros média 13.03% ao mês.
                                </p>
                            </div>
                            <div class="col-md-5 ml-auto mr-auto item">
                                <img src="img/ico-cartao-credito.gif" alt="Cartão de crédito" title="Cartão de crédito">
                                <p>
                                    <strong>Cartão de crédito</strong>
                                    Quando é paga somente a quantia mínima da fatura, o restante pode ser considerado empréstimo para o banco que mandará o valor restante no mês seguinte acrescido de juros. Taxa de juros média 14.33% ao mês.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10 ml-auto mr-auto">
                        <div class="row">
                            <div class="col-md-5 ml-auto mr-auto item">
                                <img src="img/ico-credito-consignado.gif" alt="Crédito consignado" title="Crédito consignado">
                                <p>
                                    <strong>Crédito consignado</strong>
                                    Funciona da mesma forma que o pessoal, porém vinculado ao salário ou benefício do INSS (Instituto Nacional do Seguro Social). O valor das parcelas do empréstimo são descontados diretamente dos recebimentos. Taxa de juros média 2.05% ao mês.
                                </p>
                            </div>
                            <div class="col-md-5 ml-auto mr-auto item">
                                <img src="img/ico-emprestimo-imovel-garantia.gif" alt="Empréstimo com imóvel em garantia" title="Empréstimo com imóvel em garantia">
                                <p>
                                    <strong>Empréstimo com imóvel em garantia</strong>
                                    Também chamado Home Equity ou refinanciamento de imóvel, na modalidade a instituição oferece até 60% do valor do imóvel residencial, comercial ou terreno que é dado como garantia. Taxa de juros média 1.39% ao mês.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- DIFERENÇAS -->

        <!-- MITOS -->
        <div class="container" id="mitos">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto text-center">
                    <h3>
                        Mitos e verdades sobre o crédito com garantia de imóvel
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-10 ml-auto mr-auto">
                    <p>Para desmistificar a modalidade, alguns dos principais mitos e verdades sobre o empréstimo com garantia de imóvel.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-10 ml-auto mr-auto">
                    <div class="row">
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-check-circle green"></i>
                                    <span>A propriedade não precisa estar quitada</span>
                                </dt>
                                <dd>
                                    Verdade: quando existe débito é feito um refinanciamento de imóvel e a instituição concede o empréstimo com garantia para o solicitante, onde parte do crédito é usado para quitar as parcelas restantes da propriedade.
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-times-circle red"></i>
                                    <span>Os juros são altos</span>
                                </dt>
                                <dd>
                                    Mito: comparado às outras modalidades, o crédito com garantia de imóvel possuí um dos menores juros do mercado. As instituições refletem a confiança diminuindo a taxa que opera de 1.15% a 2.99% ao mês.
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-10 ml-auto mr-auto">
                    <div class="row">
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-check-circle green"></i>
                                    <span>É necessário comprovar renda</span>
                                </dt>
                                <dd>
                                        Verdade: para garantir a segurança para a instituição e para o beneficiado, é necessária a comprovação de renda e apresentação dos documentos da propriedade para a liberação do refinanciamento de imóvel.
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-times-circle red"></i>
                                    <span>Não pode vender o imóvel</span>
                                </dt>
                                <dd>
                                    Mito: é possível negociar o imóvel. Se ambas as partes estiverem de acordo e o novo dono assumir o restante do valor do empréstimo com garantia a instituição realiza a transferência do débito.
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-10 ml-auto mr-auto">
                    <div class="row">
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-check-circle green"></i>
                                    <span>O empréstimo será de até 60% do valor do imóvel </span>
                                </dt>
                                <dd>
                                    Verdade: por determinação do órgão regulador, todo empréstimo com garantia de imóvel não deve ultrapassar 60% do valor da propriedade.<br>
                                    A norma é para proteger tanto o beneficiado quanto a instituição. O valor é definido através de avaliação do imóvel e sempre é feita por empresa terceirizada considerando os valores de mercado para a liberação do crédito.
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-times-circle red"></i>
                                    <span>O crédito com garantia de imóvel é liberado para fins específicos</span>
                                </dt>
                                <dd>
                                    Mito: o uso do termo refinanciamento de imóvel não significa que o valor adquirido é destinado a compra de uma casa, por exemplo. Com o crédito do empréstimo com garantia ou ao refinanciar imóvel, o valor pode ser usado para qualquer finalidade, como: viagens, compras, investimentos e etc. 
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-10 ml-auto mr-auto">
                    <div class="row">
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-check-circle green"></i>
                                    <span>As prestações fixas são mais seguras</span>
                                </dt>
                                <dd>
                                    Verdade: empréstimos com garantia de imóvel são influenciados pela Taxa Referencial (TR é um índice de correção do saldo devedor), assim como financiamentos imobiliários e modalidades de investimentos. Porém, diferente destes, o empréstimo com imóvel em garantia também possui como característica um fator que impede que a amortização suba mais que a inflação ou a correção média dos salários, o que minimiza oscilações em seus valores e torna mais prático o planejamento pessoal.
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-times-circle red"></i>
                                    <span>A intenção da financeira é ficar com o seu imóvel </span>
                                </dt>
                                <dd>
                                    Mito: o temor de grande parte dos proprietários em se tratando da modalidade de empréstimo com garantia de imóvel é justamente perder a propriedade. Mas, a alienação raramente acontece, e antes as instituições tendem a esgotar as possibilidades de renegociação de dívida.<br>
                                    O empréstimo com garantia é mais vantajoso e seguro principalmente pela existência de um nível de confiança maior entre todas as partes. O imóvel é como uma rede de proteção, é muito importante que esteja ali, mas a intenção é de nunca usá-la.
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-10 ml-auto mr-auto">
                    <div class="row">
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-question-circle yellow"></i>
                                    <span>Financiamentos diretos são mais produtivos </span>
                                </dt>
                                <dd>
                                    Depende: incorporadoras de crédito tendem a liberar o dinheiro mais facilmente, são mais flexíveis com a documentação e comprovação de renda. Porém o home equity feito por financeiras possui taxas bem menores devido ao tempo que o beneficiado tem para quitar o empréstimo com imóvel de garantia (pode chegar a 20 anos). Adicione o fato de incorporadoras pedirem entradas superiores às exigidas por financeiras. 
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-5 ml-auto mr-auto">
                            <dl>
                                <dt>
                                    <i class="fas fa-times-circle red"></i>
                                    <span>É muito burocrático</span>
                                </dt>
                                <dd>
                                    Mito: a burocracia para contratação do home equity tende a ser a mesma adotada em outras solicitações de crédito, apenas incluindo os documentos do imóvel.
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn-default">
                        Solicitar Proposta
                    </button>
                </div>
            </div>
        </div>
        <!-- MITOS -->

        <!-- PRÓS E CONTRAS -->
        <div class="container-fluid" id="pros-contras">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-12 ml-auto mr-auto">
                        <h3>
                            Prós e contras de obter crédito com garantia de imóvel
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 col-12 ml-auto mr-auto">
                        <div class="row">
                            <div class="col-sm-6 ml-auto text-center item">
                                <img src="img/ico-pros.gif" alt="Prós" title="Prós">
                                <p><strong>Prós</strong></p>
                                <ul>
                                    <li>Mais tempo para pagar (até 20 anos)</li>
                                    <li>Créditos mais altos (até 60% do valor do imóvel).</li>
                                    <li>Juros menores (1.15% a 2.99% ao mês).</li>
                                </ul>
                            </div>
                            <div class="col-sm-5 mr-auto text-center item">
                                <img src="img/ico-contras.gif" alt="Contras" title="Contras">
                                <p><strong>Contras</strong></p>
                                <ul>
                                    <li>Imóveis livres de hipoteca ou alienação.</li>
                                    <li>Valor mínimo para contratar.</li>
                                    <li>Planejamento a longo prazo.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PRÓS E CONTRAS -->

        <!-- CONTRATAR -->
        <div class="container" id="contratar">
            <div class="row">
                <div class="col-md-6 col-10 ml-auto mr-auto">
                    <h3>
                        Como contratar empréstimo com garantia de imóvel?
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-10 ml-auto mr-auto">
                    <p>
                        Solicitar crédito com garantia de imóvel, já conhecido home equity ou refinanciamento de imóvel, funciona de forma semelhante a qualquer solicitação de crédito, apenas com a inclusão de documentos referentes a propriedade.
                    </p>
                    <h4>
                        Para adquirir são necessários alguns passos, são eles:
                    </h4>
                    <img src="img/img-passos.gif" alt="" title="">
                </div>
            </div>
        </div>
        <!-- CONTRATAR -->

        <!-- SIMULAÇÃO -->
        <div class="container-fluid" id="simulacao">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-10 ml-auto mr-auto">
                        <h3>
                            Simulação do empréstimo com garantia de imóvel
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10 ml-auto mr-auto">
                        <p>
                            Grande parte das instituições que trabalham com a modalidade de empréstimo com garantia, oferecem simulações ao interessado, é prudente optar por aquelas que oferecem um sistema exclusivo de simulação de crédito com garantia de imóvel online e descomplicado.
                        </p>
                        <p>
                            Para consultar as melhores condições para o seu empréstimo com garantia de imóvel realize a simulação. 
                        </p>
                        <button class="btn-simular">
                            Simular empréstimo
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIMULAÇÃO -->

        <!-- DOCUMENTAÇÃO -->
        <div class="container-fluid" id="documentacao">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-10 ml-auto mr-auto">
                        <h3>
                            Documentos para aprovação do crédito com garantia de imóvel
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-12 ml-auto mr-auto">
                        <p>Como em qualquer solicitação de crédito, é necessário que o futuro beneficiado apresente alguns documentos para análise e liberação dos valores, são eles:</p>
                        <ul>
                            <li>
                                <i class="fas fa-check-circle"></i>
                                <span>Documento de identidade</span>
                            </li>
                            <li>
                                <i class="fas fa-check-circle"></i>
                                <span>CPF</span>
                            </li>
                            <li>
                                <i class="fas fa-check-circle"></i>
                                <span>Comprovante de estado civil e documentos do cônjuge, se houver</span>
                            </li>
                            <li>
                                <i class="fas fa-check-circle"></i>
                                <span>Comprovante de residência</span>
                            </li>
                            <li>
                                <i class="fas fa-check-circle"></i>
                                <span>Comprovantes de renda</span>
                            </li>
                            <li>
                                <i class="fas fa-check-circle"></i>
                                <span>Matrícula atualizada e outros documentos do imóvel</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 col-12 ml-auto mr-auto">
                        <h4>
                            Prazo para liberação do crédito com garantia de imóvel
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-12 ml-auto mr-auto">
                        <div class="row">
                            <div class="col-lg-12 prazo">
                                <img src="img/ico-documentacao.gif" alt="Documentação" title="Documentação">
                                <p>Após entrega da documentação e assinatura do contrato, o empréstimo com garantia de imóvel é liberado de 4 à 8 semanas.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- DOCUMENTAÇÃO -->

        <!-- CUIDADOS -->
        <div class="container-fluid" id="cuidados">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-10 ml-auto mr-auto">
                        <h3>
                            Cuidados ao adquirir empréstimo com garantia de imóvel
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mr-auto ml-auto">
                        <div class="row mb-5">
                            <div class="col-lg-5 col-sm-6 item text-center ml-auto mr-auto">
                                <img src="img/ico-questione-pag.gif">
                                <h4>Questione pagamentos antecipados</h4>
                                <p>
                                    Não é comum que instituições solicitem pagamentos antecipados para liberação do crédito com garantia de imóvel. Por isso, mesmo em caso de refinanciamento de imóveis, onde parte do valor concedido é utilizado para quitação dos débitos da propriedade, o abatimento é feito com o próprio valor liberado.
                                </p>
                            </div>
                            <div class="col-lg-5 col-sm-6 item ml-auto mr-auto text-center">
                                <img src="img/ico-questione-analise.gif">
                                <h4>Questione a cobrança de taxas para análise de crédito</h4>
                                <p>
                                        Fique atento a cobranças de taxas indevidas como para análise de crédito. Simulações e estudos da documentação dos interessados na obtenção do crédito não geram encargos.
                                </p>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-lg-5 col-sm-6 item ml-auto mr-auto text-center">
                                <img src="img/ico-pesquise.gif">
                                <h4>Pesquise a instituição </h4>
                                <p>
                                        Conheça bem com quem está negociando, busque auxílio e sane todas as suas dúvidas antes de assinar o contrato de empréstimo com imóvel em garantia.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn-default">
                                    Solicitar Proposta
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CUIDADOS -->

        <!-- PARCEIROS -->
        <div class="container-fluid" id="parceiros">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-xl-11 mr-auto ml-auto">
                        <h3>
                            Conheça nossos parceiros
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-xl-8 mr-auto ml-auto">
                        <div class="row justify-content-md-center">
                            <div class="col-sm-4 text-center">
                                <img class="img-fluid lgo-novi" src="img/logo-novi.gif" alt="NOVI - Soluções financeiras" title="NOVI - Soluções financeiras" />
                            </div>
                            <div class="col-sm-4 text-center">
                                <img class="img-fluid lgo-barigui" src="img/logo-barigui.gif" alt="Barigui Conglomerado Financeiro" title="Barigui Conglomerado Financeiro" />
                            </div>
                            <div class="col-sm-4 text-center">
                                <img class="img-fluid lgo-creditas" src="img/logo-creditas.gif" alt="Creditas" title="Creditas" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PARCEIROS -->

        <!-- AVISO -->
        <div class="container" id="aviso">
            <div class="row">
                <div class="col-lg-10 col-12 ml-auto mr-auto">
                    <p>O Agente Imóvel é uma plataforma digital que visa facilitar a contratação de empréstimo com garantia de imóvel no papel de <a href="https://www.bcb.gov.br/pre/bc_atende/port/correspondentes.asp" target="_blank">correspondente bancário</a>. Seguimos rigorosamente as diretrizes estabelecidas na <a href="https://www.bcb.gov.br/pre/normativos/busca/normativo.asp?tipo=res&ano=2011&numero=3954" target="_blank">resolução n 3.954/11</a> do <a href="https://www.bcb.gov.br/" target="_blank">Banco Central do Brasil</a> de 24 de fevereiro de 2011.</p>
                    <p>Atuamos em parceria com as instituições: Creditas Soluções Financeiras LTDA - CNPJ/MF 17.770.708/0001-24, Barigui Cia. Hipotecária - CNPJ 14.511.781/0001-93, Barigui S/A Crédito 00.556.603/0001-74 e Novi Soluções Financeiras LTDA - CNPJ 14.276.481/0001-77.</p>
                    <p>Para mais informações sobre correspondente bancário, consulte: <a href="https://www.certificacaofebraban.org.br/Home/Correspondentes" target="_blank">Fenabran - correspondente bancário</a>.</p>
                    <p>Para mais informações sobre estatísticas econômicas e bancárias, consulte: <a href="https://portal.febraban.org.br/paginas/21/pt-br/#" target="_blank">Portal Fenabran</a>.</p>
                    <p>Para informações sobre a lei 9.514/97 do Código de Defesa do Consumidor relacionada ao crédito com imóvel, consulte: <a href="http://www.planalto.gov.br/ccivil_03/LEIS/L9514.htm" target="_blank">Planalto - Lei de 9.514/97</a>.</p>
                    <p>Agente Imóvel Inteligência de Busca - CNPJ 13.353.999/0001-02, sede na Av. Cidade Jardim, 377 - Itaim Bibi - São Paulo.</p>
                    <img src="img/selo.png" alt="Você está em um ambiente seguro" title="Você está em um ambiente seguro">
                </div>
            </div>
        </div>
        <!-- AVISO -->

        <!-- FOOTER --->
        <footer>
            <div class="top_section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 ft-cnt ft-left">
                            <a href="/" class="ft-logo"></a>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 ft-cnt ft-mid">
                            <p class="ft-main-title">ANUNCIE SEUS IMÓVEIS</p>
                            <p class="txt">Fácil, rápido e com excelentes resultados.</p>
                            <a href="/anuncie-imovel/">
                                <button type="button" class="btn btn-orange">Saiba mais</button>
                            </a>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 ft-cnt ft-right">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 ft-links">
                                    <p class="ft-title">LINKS</p>
                                    <p><a href="/sobre-nos/">Sobre nós</a></p>
                                    <p><a href="/centro-de-ajuda-fale-conosco/">Fale conosco</a></p>
                                    <p><a href="/noticias/">Notícias</a></p>
                                    <p><a href="https://vagas.agenteimovel.com.br">Vagas</a></p>
                                </div>
                                <div class="col-xs-6 col-sm-6 social-btns">
                                    <p class="ft-title">SIGA-NOS</p>
                                    <p><a href="https://www.facebook.com/agenteimovel" class="sb-fb"></a> <a href="https://www.twitter.com/agenteimovel" class="sb-tw"></a></p>
                                    <p><a href="https://pinterest.com/agenteimovel/" class="sb-pi"></a> <a href="https://plus.google.com/+AgenteimovelBr/posts" class="sb-gp"></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom_section">
                <div class="container">
                    <p>© Agente Imóvel. Todos os direitos reservados. <a href="/privacidade">Política de privacidade</a>
                    </p>

                </div>
            </div>
        </footer>
        <!-- FOOTER --->

        <script src="js/plugin/jquery-3.3.1.min.js"></script>
        <script src="js/plugin/bootstrap.min.js"></script>
        <script src="js/plugin/jquery.cycle2.min.js"></script>
        <script src="js/plugin/rangeslider.min.js"></script>
        <script src="js/functions.js"></script>

    </body>

</html>